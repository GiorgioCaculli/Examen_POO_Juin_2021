package modele;

import java.util.ArrayList;
import java.util.List;

import exception.DoublonExceptionCaculli2;

public class GestionListeCaculli2
{
    private List< LivreCaculli2 > livresCaculli2;
    
    public GestionListeCaculli2()
    {
        livresCaculli2 = new ArrayList<>();
    }
    
    public boolean addCaculli2( LivreCaculli2 lCaculli2 )
    {
        try
        {
            if( lCaculli2 == null )
            {
                throw new NullPointerException();
            }
            if( livresCaculli2.contains( lCaculli2 ) )
            {
                throw new DoublonExceptionCaculli2();
            }
        }
        catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        catch (DoublonExceptionCaculli2 e) {
            e.printStackTrace();
            return false;
        }
        return livresCaculli2.add( lCaculli2.clone() );
    }
    
    public boolean removeCaculli2( LivreCaculli2 lCaculli2 )
    {
        if( lCaculli2 == null )
        {
            return false;
        }
        return livresCaculli2.remove( lCaculli2 );
    }

    public boolean update( LivreCaculli2 l1Caculli2, LivreCaculli2 l2Caculli2 )
    {
        if( livresCaculli2.indexOf( l1Caculli2 ) != -1 && !livresCaculli2.contains( l2Caculli2 ) )
        {
            livresCaculli2.set( livresCaculli2.indexOf( l1Caculli2 ), l2Caculli2.clone() );
            return true;
        }
        return false;
    }
    
    public List< LivreCaculli2 > getLivresCaculli2()
    {
        List< LivreCaculli2 > tmpCaculli2 = new ArrayList<>();
        for( LivreCaculli2 lCaculli2 : livresCaculli2 )
        {
            tmpCaculli2.add( lCaculli2.clone() );
        }
        return tmpCaculli2;
    }
    
    public String toString()
    {
        StringBuilder sbCaculli2 = new StringBuilder();
        for( LivreCaculli2 lCaculli2 : livresCaculli2 )
        {
            sbCaculli2.append( lCaculli2 ).append( "\n" );
        }
        return sbCaculli2.toString();
    }
    
    public GestionListeCaculli2 clone()
    {
        GestionListeCaculli2 tmp = new GestionListeCaculli2();
        for( LivreCaculli2 lCaculli2 : livresCaculli2 )
        {
            tmp.addCaculli2( lCaculli2.clone() );
        }
        return tmp;
    }
}
