package modele;

import java.util.ArrayList;
import java.util.List;

public class LivreCaculli2
{
    private String titreCaculli2, nomAuteurCaculli2, prenomAuteurCaculli2;
    private int anneeCaculli2;
    private GenreCaculli2 genreCaculli2;
    private LangueCaculli2 langueCaculli2;
    private List< FormatsCaculli2 > formatsCaculli2;

    public LivreCaculli2( String titreCaculli2, String nomAuteurCaculli2, String prenomAuteurCaculli2,
            int anneeCaculli2, GenreCaculli2 genreCaculli2, LangueCaculli2 langueCaculli2,
            List< FormatsCaculli2 > formatsCaculli2 )
    {
        setTitreCaculli2( titreCaculli2 );
        setNomAuteurCaculli2( nomAuteurCaculli2 );
        setPrenomAuteurCaculli2( prenomAuteurCaculli2 );
        setAnneeCaculli2( anneeCaculli2 );
        setGenreCaculli2( genreCaculli2 );
        setLangueCaculli2( langueCaculli2 );
        setFormatsCaculli2( formatsCaculli2 );
    }

    public void setTitreCaculli2( String titreCaculli2 )
    {
        this.titreCaculli2 = titreCaculli2;
    }

    public String getTitreCaculli2()
    {
        return titreCaculli2;
    }

    public void setNomAuteurCaculli2( String nomAuteurCaculli2 )
    {
        this.nomAuteurCaculli2 = nomAuteurCaculli2;
    }

    public String getNomAuteurCaculli2()
    {
        return nomAuteurCaculli2;
    }

    public void setPrenomAuteurCaculli2( String prenomAuteurCaculli2 )
    {
        this.prenomAuteurCaculli2 = prenomAuteurCaculli2;
    }

    public String getPrenomAuteurCaculli2()
    {
        return prenomAuteurCaculli2;
    }

    public void setAnneeCaculli2( int anneeCaculli2 )
    {
        if ( anneeCaculli2 < 0 )
        {
            this.anneeCaculli2 = 0;
            return;
        }
        this.anneeCaculli2 = anneeCaculli2;
    }

    public int getAnneeCaculli2()
    {
        return anneeCaculli2;
    }

    public void setGenreCaculli2( GenreCaculli2 genreCaculli2 )
    {
        this.genreCaculli2 = genreCaculli2;
    }

    public GenreCaculli2 getGenreCaculli2()
    {
        return genreCaculli2;
    }

    public void setLangueCaculli2( LangueCaculli2 langueCaculli2 )
    {
        this.langueCaculli2 = langueCaculli2;
    }

    public LangueCaculli2 getLangueCaculli2()
    {
        return langueCaculli2;
    }

    public void setFormatsCaculli2( List< FormatsCaculli2 > formatsCaculli2 )
    {
        this.formatsCaculli2 = formatsCaculli2;
    }

    public List< FormatsCaculli2 > getFormatsCaculli2()
    {
        List< FormatsCaculli2 > tmpCaculli2 = new ArrayList<>();
        for ( FormatsCaculli2 fCaculli2 : formatsCaculli2 )
        {
            tmpCaculli2.add( fCaculli2 );
        }
        return tmpCaculli2;
    }

    public boolean equals( Object oCaculli2 )
    {
        if ( oCaculli2 instanceof LivreCaculli2 )
        {
            LivreCaculli2 tmpCaculli2 = ( LivreCaculli2 ) oCaculli2;
            return tmpCaculli2.getTitreCaculli2().equalsIgnoreCase( titreCaculli2 );
        }
        return false;
    }

    public LivreCaculli2 clone()
    {
        return new LivreCaculli2( titreCaculli2, nomAuteurCaculli2, prenomAuteurCaculli2, anneeCaculli2, genreCaculli2,
                langueCaculli2, formatsCaculli2 );
    }

    public String toString()
    {
        StringBuilder sbCaculli2 = new StringBuilder();
        sbCaculli2.append( String.format( "Titre: %s - Auteur: %s %s - Annee: %d - Genre: %s - Langue: %s - Formats: ",
                titreCaculli2, nomAuteurCaculli2, prenomAuteurCaculli2, anneeCaculli2, genreCaculli2,
                langueCaculli2 ) );
        for ( FormatsCaculli2 fCaculli2 : formatsCaculli2 )
        {
            sbCaculli2.append( String.format( "%s ", fCaculli2 ) );
        }
        return sbCaculli2.toString();
    }
}
