package exception;

public class DonneesIncompletesExceptionCaculli2 extends Exception
{
    public DonneesIncompletesExceptionCaculli2()
    {
        super( "Donnees incompatibles" );
    }
}
