package application;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import exception.DonneesIncompletesExceptionCaculli2;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import modele.FormatsCaculli2;
import modele.GenreCaculli2;
import modele.GestionListeCaculli2;
import modele.LangueCaculli2;
import modele.LivreCaculli2;

public class PaneEncodageBP extends BorderPane
{
    private GestionListeCaculli2 gestionListeCaculli2;
    private TableView< LivreCaculli2 > tvLivresCaculli2;
    private GridPane gpCaculli2;
    private Label labelTitreCaculli2;
    private TextField textFieldTitreCaculli2;
    private Label labelNomCaculli2;
    private TextField textFieldNomCaculli2;
    private Label labelPrenomCaculli2;
    private TextField textFieldPrenomCaculli2;
    private Label labelAnneeCaculli2;
    private TextField textFieldAnneeCaculli2;
    private ChoiceBox< LangueCaculli2 > choiceBoxLangueCaculli2;
    private Label labelGenreCaculli2;
    private Label labelFormatCaculli2;
    private Button boutonAjouterCaculli2;
    private Button boutonEffacerCaculli2;
    private ToggleGroup toggleGroupRadioBoutonCaculli2;
    private RadioButton radioBoutonRomanCaculli2;
    private RadioButton radioBoutonEssaiCaculli2;
    private RadioButton radioBoutonBiographieCaculli2;
    private RadioButton radioBoutonDictionnaireCaculli2;
    private CheckBox checkBoxRelieCaculli2;
    private CheckBox checkBoxBrocheCaculli2;
    private CheckBox checkBoxElectroniqueCaculli2;

    public PaneEncodageBP( GestionListeCaculli2 gestionListeCaculli2, TableView< LivreCaculli2 > tvLivresCaculli2 )
    {
        this.gestionListeCaculli2 = gestionListeCaculli2;
        this.tvLivresCaculli2 = tvLivresCaculli2;

        setCenter( getGPCaculli2() );
        HBox buttonBoxCaculli2 = new HBox();
        buttonBoxCaculli2.getChildren().add( getBoutonAjouterCaculli2() );
        buttonBoxCaculli2.getChildren().add( getBoutonEffacerCaculli2() );
        setBottom( buttonBoxCaculli2 );
    }

    public GridPane getGPCaculli2()
    {
        if ( gpCaculli2 == null )
        {
            getTGCaculli2();
            gpCaculli2 = new GridPane();
            gpCaculli2.add( getLabelTitreCaculli2(), 0, 0 );
            gpCaculli2.add( getTextFieldTitreCaculli2(), 1, 0 );
            gpCaculli2.add( getLabelAnneeCaculli2(), 2, 0 );
            gpCaculli2.add( getTextFieldAnneeCaculli2(), 3, 0 );
            gpCaculli2.add( getChoiceBoxLangueCaculli2(), 4, 0 );
            gpCaculli2.add( getLabelNomCaculli2(), 0, 1 );
            gpCaculli2.add( getTextFieldNomCaculli2(), 1, 1 );
            gpCaculli2.add( getLabelPrenomCaculli2(), 0, 2 );
            gpCaculli2.add( getTextFieldPrenomCaculli2(), 1, 2 );
            gpCaculli2.add( getLabelGenreCaculli2(), 0, 3 );
            gpCaculli2.add( getRBRomanCaculli2(), 0, 4 );
            gpCaculli2.add( getRBEssaiCaculli2(), 0, 5 );
            gpCaculli2.add( getRBBiographieCaculli2(), 0, 6 );
            gpCaculli2.add( getRBDictionnaireCaculli2(), 0, 7 );
            gpCaculli2.add( getLabelFormatCaculli2(), 2, 3 );
            gpCaculli2.add( getCheckBoxRelieCaculli2(), 2, 4 );
            gpCaculli2.add( getCheckBoxBrocheCaculli2(), 2, 5 );
            gpCaculli2.add( getCheckBoxElectroniqueCaculli2(), 2, 6 );
        }
        return gpCaculli2;
    }

    public Button getBoutonAjouterCaculli2()
    {
        if ( boutonAjouterCaculli2 == null )
        {
            boutonAjouterCaculli2 = new Button( "Ajouter" );
            boutonAjouterCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {

                @Override
                public void handle( ActionEvent arg0 )
                {
                    try
                    {
                        Integer.parseInt( textFieldAnneeCaculli2.getText().trim() );
                        if ( textFieldTitreCaculli2.getText().trim().isEmpty()
                                || textFieldNomCaculli2.getText().trim().isEmpty()
                                || textFieldPrenomCaculli2.getText().trim().isEmpty()
                                || toggleGroupRadioBoutonCaculli2.getSelectedToggle() == null
                                || choiceBoxLangueCaculli2.getValue() == null )
                        {
                            throw new DonneesIncompletesExceptionCaculli2();
                        }
                        else
                        {
                            List< FormatsCaculli2 > formatsCaculli2 = new ArrayList<>();
                            if ( checkBoxBrocheCaculli2.isSelected() )
                            {
                                formatsCaculli2.add( FormatsCaculli2.BROCHE );
                            }
                            if ( checkBoxElectroniqueCaculli2.isSelected() )
                            {
                                formatsCaculli2.add( FormatsCaculli2.ELECTRONIQUE );
                            }
                            if ( checkBoxRelieCaculli2.isSelected() )
                            {
                                formatsCaculli2.add( FormatsCaculli2.RELIE );
                            }
                            RadioButton radioBoutonSelecCaculli2 = ( RadioButton ) toggleGroupRadioBoutonCaculli2
                                    .getSelectedToggle();
                            LivreCaculli2 tmp = new LivreCaculli2( textFieldTitreCaculli2.getText(),
                                    textFieldNomCaculli2.getText(), textFieldPrenomCaculli2.getText(),
                                    Integer.valueOf( textFieldAnneeCaculli2.getText() ),
                                    ( GenreCaculli2 ) radioBoutonSelecCaculli2.getUserData(),
                                    choiceBoxLangueCaculli2.getValue(), formatsCaculli2 );
                            tvLivresCaculli2.getItems().add( tmp );
                            gestionListeCaculli2.addCaculli2( tmp );
                            ( ( TextField ) gpCaculli2.getChildren()
                                    .get( gpCaculli2.getChildren().indexOf( textFieldNomCaculli2 ) ) ).clear();
                            ( ( TextField ) gpCaculli2.getChildren()
                                    .get( gpCaculli2.getChildren().indexOf( textFieldPrenomCaculli2 ) ) ).clear();
                            ( ( ChoiceBox< LangueCaculli2 > ) gpCaculli2.getChildren()
                                    .get( gpCaculli2.getChildren().indexOf( choiceBoxLangueCaculli2 ) ) )
                                            .setValue( LangueCaculli2.FRANCAIS );
                            ( ( MainPaneBPCaculli2 ) getParent().getParent() )
                                    .setVisibleNode( TableViewLivresBPCaculli2.class.getSimpleName() );
                        }
                    }
                    catch ( NumberFormatException e )
                    {
                        e.printStackTrace();
                    }
                    catch ( DonneesIncompletesExceptionCaculli2 e )
                    {
                        e.printStackTrace();
                    }
                }
            } );
        }
        return boutonAjouterCaculli2;
    }

    public Button getBoutonEffacerCaculli2()
    {
        if ( boutonEffacerCaculli2 == null )
        {
            boutonEffacerCaculli2 = new Button( "Effacer" );
            boutonEffacerCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {
                @Override
                public void handle( ActionEvent arg0 )
                {
                    ( ( TextField ) gpCaculli2.getChildren()
                            .get( gpCaculli2.getChildren().indexOf( textFieldTitreCaculli2 ) ) ).clear();
                    ( ( TextField ) gpCaculli2.getChildren()
                            .get( gpCaculli2.getChildren().indexOf( textFieldAnneeCaculli2 ) ) ).clear();
                    ( ( TextField ) gpCaculli2.getChildren()
                            .get( gpCaculli2.getChildren().indexOf( textFieldNomCaculli2 ) ) ).clear();
                    ( ( TextField ) gpCaculli2.getChildren()
                            .get( gpCaculli2.getChildren().indexOf( textFieldPrenomCaculli2 ) ) ).clear();
                    ( ( ChoiceBox< LangueCaculli2 > ) gpCaculli2.getChildren()
                            .get( gpCaculli2.getChildren().indexOf( choiceBoxLangueCaculli2 ) ) )
                                    .setValue( LangueCaculli2.FRANCAIS );
                    ( ( MainPaneBPCaculli2 ) getParent().getParent() )
                            .setVisibleNode( TableViewLivresBPCaculli2.class.getSimpleName() );
                }
            } );
        }
        return boutonEffacerCaculli2;
    }

    public Label getLabelTitreCaculli2()
    {
        if ( labelTitreCaculli2 == null )
        {
            labelTitreCaculli2 = new Label( "Titre:" );
        }
        return labelTitreCaculli2;
    }

    public TextField getTextFieldTitreCaculli2()
    {
        if ( textFieldTitreCaculli2 == null )
        {
            textFieldTitreCaculli2 = new TextField();
        }
        return textFieldTitreCaculli2;
    }

    public Label getLabelNomCaculli2()
    {
        if ( labelNomCaculli2 == null )
        {
            labelNomCaculli2 = new Label( "Nom:" );
        }
        return labelNomCaculli2;
    }

    public TextField getTextFieldNomCaculli2()
    {
        if ( textFieldNomCaculli2 == null )
        {
            textFieldNomCaculli2 = new TextField();
        }
        return textFieldNomCaculli2;
    }

    public Label getLabelPrenomCaculli2()
    {
        if ( labelPrenomCaculli2 == null )
        {
            labelPrenomCaculli2 = new Label( "Prenom:" );
        }
        return labelPrenomCaculli2;
    }

    public TextField getTextFieldPrenomCaculli2()
    {
        if ( textFieldPrenomCaculli2 == null )
        {
            textFieldPrenomCaculli2 = new TextField();
        }
        return textFieldPrenomCaculli2;
    }

    public Label getLabelAnneeCaculli2()
    {
        if ( labelAnneeCaculli2 == null )
        {
            labelAnneeCaculli2 = new Label( "Annee" );
        }
        return labelAnneeCaculli2;
    }

    public TextField getTextFieldAnneeCaculli2()
    {
        if ( textFieldAnneeCaculli2 == null )
        {
            textFieldAnneeCaculli2 = new TextField();
        }
        return textFieldAnneeCaculli2;
    }

    public ChoiceBox< LangueCaculli2 > getChoiceBoxLangueCaculli2()
    {
        if ( choiceBoxLangueCaculli2 == null )
        {
            choiceBoxLangueCaculli2 = new ChoiceBox<>();
            choiceBoxLangueCaculli2
                    .setItems( FXCollections.observableArrayList( Arrays.asList( LangueCaculli2.values() ) ) );
            choiceBoxLangueCaculli2.setValue( LangueCaculli2.FRANCAIS );
        }
        return choiceBoxLangueCaculli2;
    }

    public Label getLabelGenreCaculli2()
    {
        if ( labelGenreCaculli2 == null )
        {
            labelGenreCaculli2 = new Label( "Genre:" );
        }
        return labelGenreCaculli2;
    }

    public Label getLabelFormatCaculli2()
    {
        if ( labelFormatCaculli2 == null )
        {
            labelFormatCaculli2 = new Label( "Format" );
        }
        return labelFormatCaculli2;
    }

    public ToggleGroup getTGCaculli2()
    {
        if ( toggleGroupRadioBoutonCaculli2 == null )
        {
            toggleGroupRadioBoutonCaculli2 = new ToggleGroup();
        }
        return toggleGroupRadioBoutonCaculli2;
    }

    public RadioButton getRBRomanCaculli2()
    {
        if ( radioBoutonRomanCaculli2 == null )
        {
            radioBoutonRomanCaculli2 = new RadioButton( "Roman" );
            radioBoutonRomanCaculli2.setToggleGroup( toggleGroupRadioBoutonCaculli2 );
        }
        return radioBoutonRomanCaculli2;
    }

    public RadioButton getRBEssaiCaculli2()
    {
        if ( radioBoutonEssaiCaculli2 == null )
        {
            radioBoutonEssaiCaculli2 = new RadioButton( "Essai" );
            radioBoutonEssaiCaculli2.setUserData( GenreCaculli2.ESSIES );
            radioBoutonEssaiCaculli2.setToggleGroup( toggleGroupRadioBoutonCaculli2 );
        }
        return radioBoutonEssaiCaculli2;
    }

    public RadioButton getRBBiographieCaculli2()
    {
        if ( radioBoutonBiographieCaculli2 == null )
        {
            radioBoutonBiographieCaculli2 = new RadioButton( "Biographie" );
            radioBoutonBiographieCaculli2.setUserData( GenreCaculli2.BIOGRAPHIE );
            radioBoutonBiographieCaculli2.setToggleGroup( toggleGroupRadioBoutonCaculli2 );
        }
        return radioBoutonBiographieCaculli2;
    }

    public RadioButton getRBDictionnaireCaculli2()
    {
        if ( radioBoutonDictionnaireCaculli2 == null )
        {
            radioBoutonDictionnaireCaculli2 = new RadioButton( "Dictionnaire" );
            radioBoutonDictionnaireCaculli2.setUserData( GenreCaculli2.DICTIONNAIRE );
            radioBoutonDictionnaireCaculli2.setToggleGroup( toggleGroupRadioBoutonCaculli2 );
        }
        return radioBoutonDictionnaireCaculli2;
    }

    public CheckBox getCheckBoxRelieCaculli2()
    {
        if ( checkBoxRelieCaculli2 == null )
        {
            checkBoxRelieCaculli2 = new CheckBox( "Relie" );
        }
        return checkBoxRelieCaculli2;
    }

    public CheckBox getCheckBoxBrocheCaculli2()
    {
        if ( checkBoxBrocheCaculli2 == null )
        {
            checkBoxBrocheCaculli2 = new CheckBox( "Broche" );
        }
        return checkBoxBrocheCaculli2;
    }

    public CheckBox getCheckBoxElectroniqueCaculli2()
    {
        if ( checkBoxElectroniqueCaculli2 == null )
        {
            checkBoxElectroniqueCaculli2 = new CheckBox( "Electronique" );
        }
        return checkBoxElectroniqueCaculli2;
    }
}
