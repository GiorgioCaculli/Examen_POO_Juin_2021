package application;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javafx.animation.AnimationTimer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;
import modele.GestionListeCaculli2;
import modele.LivreCaculli2;
import serialization.PersistenceCaculli2;

public class MainPaneBPCaculli2 extends BorderPane
{
    private GestionListeCaculli2 gestionListeCaculli2;
    private MenuBar topMenuBarCaculli2;
    private Menu fichierMenuCaculli2;
    private MenuItem ouvrirMenuItemCaculli2;
    private MenuItem sauvegarderMenuItemCacullI2;
    private StackPane mainPaneSPCaculli2;
    private TableViewLivresBPCaculli2 tableViewLivresCaculli2;
    private PaneEncodageBP paneEncodageCaculli2;
    private VBox vboxGaucheCaculli2;
    private Button boutonAccueilCaculli2;
    private Button boutonEncodageCaculli2;
    private Button boutonTableauCaculli2;
    private long elapsedTimeCaculli2;
    private AnimationTimer animationCaculli2;
    private LocalTime ltCaculli2 = LocalTime.now();
    private DateTimeFormatter dtfCaculli2 = DateTimeFormatter.ofPattern( "HH:mm:ss" );
    private String timeStringCaculli2 = ltCaculli2.format( dtfCaculli2 );
    private Label timeLabelCaculli2;
    private Button boutonSupprimerCaculli2;

    public MainPaneBPCaculli2( GestionListeCaculli2 gestionListeCaculli2 )
    {
        this.gestionListeCaculli2 = gestionListeCaculli2;
        setCenter( getMainPaneSPCaculli2() );
        setLeft( getVBoxGaucheCaculli2() );
        setTop( getTopMenuBarCaculli2() );

        getAnimationCaculli2().start();

        setVisibleNode( timeLabelCaculli2.getClass().getSimpleName() );
    }

    public StackPane getMainPaneSPCaculli2()
    {
        if ( mainPaneSPCaculli2 == null )
        {
            mainPaneSPCaculli2 = new StackPane();
            mainPaneSPCaculli2.getChildren().add( getTableViewLivresCaculli2() );
            mainPaneSPCaculli2.getChildren().add( getPaneEncodageCaculli2() );
            mainPaneSPCaculli2.getChildren().add( getTimeLabelCaculli2() );
        }
        return mainPaneSPCaculli2;
    }

    public void setVisibleNode( String id )
    {
        for ( Node n : mainPaneSPCaculli2.getChildren() )
        {
            if ( n.getClass().getSimpleName().equalsIgnoreCase( id ) )
            {
                n.setVisible( true );
            }
            else
            {
                n.setVisible( false );
            }
        }
    }

    public TableViewLivresBPCaculli2 getTableViewLivresCaculli2()
    {
        if ( tableViewLivresCaculli2 == null )
        {
            tableViewLivresCaculli2 = new TableViewLivresBPCaculli2( gestionListeCaculli2 );
        }
        return tableViewLivresCaculli2;
    }

    public PaneEncodageBP getPaneEncodageCaculli2()
    {
        if ( paneEncodageCaculli2 == null )
        {
            paneEncodageCaculli2 = new PaneEncodageBP( gestionListeCaculli2,
                    tableViewLivresCaculli2.getTvLivresCaculli2() );
        }
        return paneEncodageCaculli2;
    }

    public Label getTimeLabelCaculli2()
    {
        if ( timeLabelCaculli2 == null )
        {
            timeLabelCaculli2 = new Label( String.format( "Bienvenue, il est %s", timeStringCaculli2 ) );
        }
        return timeLabelCaculli2;
    }

    public AnimationTimer getAnimationCaculli2()
    {
        if ( animationCaculli2 == null )
        {
            animationCaculli2 = new AnimationTimer()
            {

                @Override
                public void handle( long nowCaculli2 )
                {
                    if ( nowCaculli2 - elapsedTimeCaculli2 > 1000000000 )
                    {
                        ltCaculli2 = LocalTime.now();
                        timeStringCaculli2 = ltCaculli2.format( dtfCaculli2 );
                        ( ( Label ) mainPaneSPCaculli2.getChildren()
                                .get( mainPaneSPCaculli2.getChildren().indexOf( timeLabelCaculli2 ) ) ).setText(
                                        String.format( "Bienvenue et bon travail, il est %s", timeStringCaculli2 ) );
                        elapsedTimeCaculli2 = nowCaculli2;
                    }

                }
            };
        }
        return animationCaculli2;
    }

    public VBox getVBoxGaucheCaculli2()
    {
        if ( vboxGaucheCaculli2 == null )
        {
            vboxGaucheCaculli2 = new VBox();
            vboxGaucheCaculli2.getChildren().add( getBoutonAccueilCaculli2() );
            vboxGaucheCaculli2.getChildren().add( getBoutonEncodageCaculli2() );
            vboxGaucheCaculli2.getChildren().add( getBoutonTableauCaculli2() );
        }
        return vboxGaucheCaculli2;
    }

    public Button getBoutonAccueilCaculli2()
    {
        if ( boutonAccueilCaculli2 == null )
        {
            boutonAccueilCaculli2 = new Button( "Accueil" );
            boutonAccueilCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {

                @Override
                public void handle( ActionEvent arg0 )
                {
                    setVisibleNode( timeLabelCaculli2.getClass().getSimpleName() );
                }
            } );
        }
        return boutonAccueilCaculli2;
    }

    public Button getBoutonEncodageCaculli2()
    {
        if ( boutonEncodageCaculli2 == null )
        {
            boutonEncodageCaculli2 = new Button( "Encodage" );
            boutonEncodageCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {
                @Override
                public void handle( ActionEvent arg0 )
                {
                    setVisibleNode( PaneEncodageBP.class.getSimpleName() );
                }
            } );
        }
        return boutonEncodageCaculli2;
    }

    public Button getBoutonTableauCaculli2()
    {
        if ( boutonTableauCaculli2 == null )
        {
            boutonTableauCaculli2 = new Button( "Tableau des livres" );
            boutonTableauCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {

                @Override
                public void handle( ActionEvent arg0 )
                {
                    setVisibleNode( TableViewLivresBPCaculli2.class.getSimpleName() );
                }
            } );
        }
        return boutonTableauCaculli2;
    }

    public MenuBar getTopMenuBarCaculli2()
    {
        if ( topMenuBarCaculli2 == null )
        {
            topMenuBarCaculli2 = new MenuBar();
            topMenuBarCaculli2.getMenus().add( getFichierMenuCaculli2() );
        }
        return topMenuBarCaculli2;
    }

    public Menu getFichierMenuCaculli2()
    {
        if ( fichierMenuCaculli2 == null )
        {
            fichierMenuCaculli2 = new Menu( "Fichier" );
            fichierMenuCaculli2.getItems().add(
                    getOuvrirMenuItemCaculli2( gestionListeCaculli2, tableViewLivresCaculli2.getTvLivresCaculli2() ) );
            fichierMenuCaculli2.getItems().add( getSauvegarderMenuItemCaculli2( gestionListeCaculli2 ) );
        }
        return fichierMenuCaculli2;
    }

    public MenuItem getOuvrirMenuItemCaculli2( GestionListeCaculli2 gestionListeCaculli2,
            TableView< LivreCaculli2 > tvLivres )
    {
        if ( ouvrirMenuItemCaculli2 == null )
        {
            ouvrirMenuItemCaculli2 = new MenuItem( "Ouvrir" );
            ouvrirMenuItemCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {

                @Override
                public void handle( ActionEvent arg0 )
                {
                    FileChooser fcCaculli2 = new FileChooser();
                    fcCaculli2.getExtensionFilters().add( new ExtensionFilter( "JSON File", "*.json" ) );
                    Stage stageCaculli2 = ( Stage ) getScene().getWindow();
                    File fCaculli2 = fcCaculli2.showOpenDialog( stageCaculli2 );
                    if ( fCaculli2 == null )
                    {
                        return;
                    }
                    GestionListeCaculli2 glCaculli2 = PersistenceCaculli2
                            .chargerListeCaculli2( fCaculli2.getAbsolutePath() );
                    for ( LivreCaculli2 lCaculli2 : glCaculli2.getLivresCaculli2() )
                    {
                        gestionListeCaculli2.addCaculli2( lCaculli2 );
                    }
                    tvLivres.setItems( FXCollections.observableArrayList( gestionListeCaculli2.getLivresCaculli2() ) );
                    tvLivres.getSelectionModel().clearSelection();
                    System.out.println( gestionListeCaculli2 );
                }
            } );
        }
        return ouvrirMenuItemCaculli2;
    }

    public MenuItem getSauvegarderMenuItemCaculli2( GestionListeCaculli2 gestionListeCaculli2 )
    {
        if ( sauvegarderMenuItemCacullI2 == null )
        {
            sauvegarderMenuItemCacullI2 = new MenuItem( "Sauvegarder" );
            sauvegarderMenuItemCacullI2.setOnAction( new EventHandler< ActionEvent >()
            {
                @Override
                public void handle( ActionEvent arg0 )
                {
                    FileChooser fcCaculli2 = new FileChooser();
                    Stage stageCaculli2 = ( Stage ) getScene().getWindow();
                    File fCaculli2 = fcCaculli2.showSaveDialog( stageCaculli2 );
                    if ( fCaculli2 == null )
                    {
                        return;
                    }
                    if ( !fCaculli2.getName().contains( "." ) )
                    {
                        fCaculli2 = new File( fCaculli2.getAbsoluteFile() + ".json" );
                    }
                    GestionListeCaculli2 glCaculli2 = gestionListeCaculli2.clone();
                    PersistenceCaculli2.sauvegarderListeCaculli2( glCaculli2, fCaculli2.getAbsolutePath() );
                }
            } );
        }
        return sauvegarderMenuItemCacullI2;
    }
}
