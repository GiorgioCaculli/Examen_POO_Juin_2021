package application;

import javafx.application.Application;
import javafx.stage.Stage;
import modele.GestionListeCaculli2;
import javafx.scene.Scene;

public class MainCaculli2 extends Application
{
    @Override
    public void start( Stage primaryStageCaculli2 )
    {
        try
        {
            int largeurCaculli2 = 1024;
            int hauteurCaculli2 = largeurCaculli2 / 4 * 3; // Resolution en 4:3
            GestionListeCaculli2 gestionListeCaculli2 = new GestionListeCaculli2();
            MainPaneBPCaculli2 rootCaculli2 = new MainPaneBPCaculli2( gestionListeCaculli2 );
            Scene sceneCaculli2 = new Scene( rootCaculli2, largeurCaculli2, hauteurCaculli2 );
            sceneCaculli2.getStylesheets().add( getClass().getResource( "application.css" ).toExternalForm() );
            primaryStageCaculli2.setScene( sceneCaculli2 );
            primaryStageCaculli2.show();
        }
        catch ( Exception eCaculli2 )
        {
            eCaculli2.printStackTrace();
        }
    }

    public static void main( String[] args )
    {
        launch( args );
    }
}
