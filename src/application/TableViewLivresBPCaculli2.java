package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.util.converter.IntegerStringConverter;
import modele.GenreCaculli2;
import modele.GestionListeCaculli2;
import modele.LangueCaculli2;
import modele.LivreCaculli2;

public class TableViewLivresBPCaculli2 extends BorderPane
{
    private TableView< LivreCaculli2 > tvLivresCaculli2;
    private GestionListeCaculli2 gestionListeCaculli2;
    private Button boutonSupprimerCaculli2;

    public TableViewLivresBPCaculli2( GestionListeCaculli2 gestionListeCaculli2 )
    {
        this.tvLivresCaculli2 = tvLivresCaculli2;
        this.gestionListeCaculli2 = gestionListeCaculli2;
        setCenter( getTvLivresCaculli2() );
        setBottom( getBoutonSupprimerCaculli2() );
    }

    public TableView< LivreCaculli2 > getTvLivresCaculli2()
    {
        if ( tvLivresCaculli2 == null )
        {
            tvLivresCaculli2 = new TableView<>();
            tvLivresCaculli2.setEditable( true );

            tvLivresCaculli2.setColumnResizePolicy( TableView.CONSTRAINED_RESIZE_POLICY );

            TableColumn< LivreCaculli2, String > titreColCaculli2 = new TableColumn<>( "Titre" );
            titreColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "titreCaculli2" ) );
            titreColCaculli2.setCellFactory( TextFieldTableCell.< LivreCaculli2 > forTableColumn() );
            titreColCaculli2.setOnEditCommit( new EventHandler< TableColumn.CellEditEvent< LivreCaculli2, String > >()
            {
                @Override
                public void handle( CellEditEvent< LivreCaculli2, String > arg0Caculli2 )
                {
                    LivreCaculli2 tmpCaculli2 = arg0Caculli2.getRowValue().clone();
                    tmpCaculli2.setNomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    gestionListeCaculli2.update( arg0Caculli2.getRowValue(), tmpCaculli2 );
                    arg0Caculli2.getRowValue().setNomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    System.out.println( gestionListeCaculli2 );
                }

            } );

            TableColumn< LivreCaculli2, String > nomColCaculli2 = new TableColumn<>( "Nom Auteur" );
            nomColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "nomAuteurCaculli2" ) );
            nomColCaculli2.setCellFactory( TextFieldTableCell.< LivreCaculli2 > forTableColumn() );
            nomColCaculli2.setOnEditCommit( new EventHandler< TableColumn.CellEditEvent< LivreCaculli2, String > >()
            {
                @Override
                public void handle( CellEditEvent< LivreCaculli2, String > arg0Caculli2 )
                {
                    LivreCaculli2 tmpCaculli2 = arg0Caculli2.getRowValue().clone();
                    tmpCaculli2.setNomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    gestionListeCaculli2.update( arg0Caculli2.getRowValue(), tmpCaculli2 );
                    arg0Caculli2.getRowValue().setNomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    System.out.println( gestionListeCaculli2 );
                }

            } );
            TableColumn< LivreCaculli2, String > prenomColCaculli2 = new TableColumn<>( "Prenom Auteur" );
            prenomColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "prenomAuteurCaculli2" ) );
            prenomColCaculli2.setCellFactory( TextFieldTableCell.< LivreCaculli2 > forTableColumn() );
            prenomColCaculli2.setOnEditCommit( new EventHandler< TableColumn.CellEditEvent< LivreCaculli2, String > >()
            {
                @Override
                public void handle( CellEditEvent< LivreCaculli2, String > arg0Caculli2 )
                {
                    LivreCaculli2 tmpCaculli2 = arg0Caculli2.getRowValue().clone();
                    tmpCaculli2.setPrenomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    gestionListeCaculli2.update( arg0Caculli2.getRowValue(), tmpCaculli2 );
                    arg0Caculli2.getRowValue().setPrenomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    System.out.println( gestionListeCaculli2 );
                }

            } );
            TableColumn< LivreCaculli2, GenreCaculli2 > genreColCaculli2 = new TableColumn<>( "Genre" );
            genreColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "genreCaculli2" ) );
            TableColumn< LivreCaculli2, LangueCaculli2 > langueColCaculli2 = new TableColumn<>( "Langue" );
            langueColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "langueCaculli2" ) );
            TableColumn< LivreCaculli2, Integer > anneeColCaculli2 = new TableColumn<>( "Annee" );
            anneeColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "anneeCaculli2" ) );
            TableColumn< LivreCaculli2, GenreCaculli2 > formatsColCaculli2 = new TableColumn<>( "Formats" );
            formatsColCaculli2.setCellValueFactory( new PropertyValueFactory<>( "formatsCaculli2" ) );
            /*genreColCaculli2.setCellFactory( TextFieldTableCell.< LivreCaculli2 > forTableColumn() );
            genreColCaculli2.setOnEditCommit( new EventHandler< TableColumn.CellEditEvent< LivreCaculli2, String > >()
            {
                @Override
                public void handle( CellEditEvent< LivreCaculli2, String > arg0Caculli2 )
                {
                    LivreCaculli2 tmpCaculli2 = arg0Caculli2.getRowValue().clone();
                    tmpCaculli2.setPrenomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    gestionListeCaculli2.update( arg0Caculli2.getRowValue(), tmpCaculli2 );
                    arg0Caculli2.getRowValue().setPrenomAuteurCaculli2( arg0Caculli2.getNewValue() );
                    System.out.println( gestionListeCaculli2 );
                }
            
            } );*/
            /*TableColumn< LivreCaculli2, LangueCaculli2 > anneeCol = new TableColumn<>( "Annee" );
            anneeCol.setCellValueFactory( new PropertyValueFactory<>( "annee" ) );
            anneeCol.setCellFactory(
                    TextFieldTableCell.< Etudiant, Integer > forTableColumn( new IntegerStringConverter() ) );
            anneeCol.setOnEditCommit( new EventHandler< TableColumn.CellEditEvent< Etudiant, Integer > >()
            {
                @Override
                public void handle( CellEditEvent< Etudiant, Integer > arg0 )
                {
                    Etudiant tmp = arg0.getRowValue().clone();
                    tmp.setAnnee( arg0.getNewValue() );
                    gestion.update( arg0.getRowValue(), tmp );
                    arg0.getRowValue().setAnnee( arg0.getNewValue() );
                    System.out.println( gestion );
                }
            
            } );*/

            tvLivresCaculli2.getColumns().addAll( titreColCaculli2, nomColCaculli2, prenomColCaculli2,
                    genreColCaculli2, langueColCaculli2, anneeColCaculli2, formatsColCaculli2 );

            tvLivresCaculli2.getSelectionModel().setSelectionMode( SelectionMode.MULTIPLE );

            tvLivresCaculli2.getItems().addAll( gestionListeCaculli2.getLivresCaculli2() );
        }
        return tvLivresCaculli2;
    }

    public Button getBoutonSupprimerCaculli2()
    {
        if ( boutonSupprimerCaculli2 == null )
        {
            boutonSupprimerCaculli2 = new Button( "Supprimer" );
            boutonSupprimerCaculli2.setOnAction( new EventHandler< ActionEvent >()
            {
                @Override
                public void handle( ActionEvent arg0 )
                {
                    ObservableList< LivreCaculli2 > selectedCaculli2 = tvLivresCaculli2.getSelectionModel()
                            .getSelectedItems();
                    for ( LivreCaculli2 lCaculli2 : gestionListeCaculli2.getLivresCaculli2() )
                    {
                        gestionListeCaculli2.removeCaculli2( lCaculli2 );
                    }
                    tvLivresCaculli2
                            .setItems( FXCollections.observableArrayList( gestionListeCaculli2.getLivresCaculli2() ) );
                    tvLivresCaculli2.getSelectionModel().clearSelection();
                    System.out.println( gestionListeCaculli2 );
                }
            } );
        }
        return boutonSupprimerCaculli2;
    }
}
