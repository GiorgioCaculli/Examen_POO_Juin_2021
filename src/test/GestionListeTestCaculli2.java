package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modele.FormatsCaculli2;
import modele.GenreCaculli2;
import modele.GestionListeCaculli2;
import modele.LangueCaculli2;
import modele.LivreCaculli2;

class GestionListeTestCaculli2
{

    private static GestionListeCaculli2 gestionListeCaculli2;
    private static LivreCaculli2 l1Caculli2, l2Caculli2, l3Caculli2, l4Caculli2;

    @BeforeAll
    static void initAll()
    {
        gestionListeCaculli2 = new GestionListeCaculli2();
    }

    @BeforeEach
    void init()
    {
        l1Caculli2 = new LivreCaculli2( "The Holy Bible", "Jesus", "Of Nazareth", 0, GenreCaculli2.BIOGRAPHIE,
                LangueCaculli2.ANGLAIS,
                new ArrayList< FormatsCaculli2 >( List.of( FormatsCaculli2.BROCHE, FormatsCaculli2.ELECTRONIQUE ) ) );
        l2Caculli2 = l1Caculli2.clone();
        l3Caculli2 = new LivreCaculli2( "Metro 2033", "Glukhovski", "Dimitry", 2021, GenreCaculli2.ESSIES,
                LangueCaculli2.ALLEMAND,
                new ArrayList< FormatsCaculli2 >( List.of( FormatsCaculli2.BROCHE, FormatsCaculli2.ELECTRONIQUE ) ) );
        l4Caculli2 = new LivreCaculli2( "The Encyclopedia", "Who", "Knows", 1989, GenreCaculli2.DICTIONNAIRE,
                LangueCaculli2.ANGLAIS, new ArrayList< FormatsCaculli2 >(
                        List.of( FormatsCaculli2.BROCHE, FormatsCaculli2.ELECTRONIQUE, FormatsCaculli2.RELIE ) ) );
    }

    @Test
    public void testAjoutLivreCaculli2()
    {
        assertTrue( () -> gestionListeCaculli2.addCaculli2( l1Caculli2 ), "echec - le livre n'a pas ete ajoute" );
        assertTrue( () -> gestionListeCaculli2.addCaculli2( l3Caculli2 ), "echec - le livre n'a pas ete ajoute" );
        assertTrue( () -> gestionListeCaculli2.addCaculli2( l4Caculli2 ), "echec - le livre n'a pas ete ajoute" );
    }

    @Test
    public void testAjoutDoublonCaculli2()
    {
        assertFalse( () -> gestionListeCaculli2.addCaculli2( l2Caculli2 ), "echec - le livre a ete ajoute" );
    }

    @Test
    public void testAjoutNullCaculli2()
    {
        assertFalse( () -> gestionListeCaculli2.addCaculli2( null ), "echec - le livre a ete ajoute" );
    }

    @Test
    public void testSuppressionLivreCaculli2()
    {
        assertTrue( () -> gestionListeCaculli2.removeCaculli2( l1Caculli2 ), "echec - le livre n'a pas ete supprime" );
    }

    @Test
    public void testSuppressionLivreNonExistantCaculli2()
    {
        assertFalse( () -> gestionListeCaculli2.removeCaculli2( l2Caculli2 ), "echec - le livre a ete supprime" );
    }

    @Test
    public void testSuppressionNullCaculli2()
    {
        assertFalse( () -> gestionListeCaculli2.removeCaculli2( null ), "echec - le livre a ete supprime" );
    }
    
    @Test
    public void testMemeListeLivresCaculli2()
    {
        GestionListeCaculli2 tmp = gestionListeCaculli2.clone();
        assertEquals( gestionListeCaculli2.getLivresCaculli2(), tmp.getLivresCaculli2(),
                "echec - les listes ne sont pas les memes" );
        assertEquals( gestionListeCaculli2.toString(), tmp.toString(), "echec - les resultats ecrits ne sont pas les memes" );
    }

    @AfterEach
    void tearDown()
    {
    }

    @AfterAll
    static void tearDownAll()
    {
        gestionListeCaculli2 = null;
        l1Caculli2 = null;
        l2Caculli2 = null;
        l3Caculli2 = null;
        l4Caculli2 = null;
    }

}
