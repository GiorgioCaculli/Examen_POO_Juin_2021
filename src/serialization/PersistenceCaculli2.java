package serialization;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import modele.GestionListeCaculli2;

public class PersistenceCaculli2
{
    private static GsonBuilder gbCaculli2 = new GsonBuilder();

    private static Gson gsonCaculli2;

    public static void sauvegarderListeCaculli2( GestionListeCaculli2 gestionCaculli2, String fileNameCaculli2 )
    {
        gbCaculli2.setPrettyPrinting();
        String pathCaculli2 = fileNameCaculli2;
        File jsonCaculli2 = null;
        try
        {
            jsonCaculli2 = new File( pathCaculli2 );
            jsonCaculli2.createNewFile();
        }
        catch ( Exception eCaculli2 )
        {
            eCaculli2.printStackTrace();
        }

        try ( /*Writer w = new FileWriter( json )*/
                OutputStreamWriter wCaculli2 = new OutputStreamWriter( new FileOutputStream( jsonCaculli2 ),
                        Charset.forName( "UTF-8" ).newEncoder() ) )
        {
            gbCaculli2.setPrettyPrinting();
            gsonCaculli2 = gbCaculli2.create();
            gsonCaculli2.toJson( gestionCaculli2, wCaculli2 );
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    public static void sauvegarderListeCaculli2( GestionListeCaculli2 gestionCaculli2 )
    {
        sauvegarderListeCaculli2( gestionCaculli2, "listeCaculli2.json" );
    }

    public static GestionListeCaculli2 chargerListeCaculli2( String pathCaculli2 )
    {
        GestionListeCaculli2 gestionCaculli2 = null;
        gsonCaculli2 = gbCaculli2.create();
        try
        {
            gestionCaculli2 = gsonCaculli2.fromJson( new FileReader( pathCaculli2, Charset.forName( "UTF-8" ) ), GestionListeCaculli2.class );
        }
        catch ( JsonSyntaxException | JsonIOException | IOException eCaculli2 )
        {
            eCaculli2.printStackTrace();
        }
        return gestionCaculli2;
    }

}
